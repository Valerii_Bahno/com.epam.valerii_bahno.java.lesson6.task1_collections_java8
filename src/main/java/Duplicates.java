import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Duplicates {

    private final String fileName = "text";
    private final String delimitersInText = "[\\s+., ]+";
    private final int duplicatesWordsQuantity = 3;

    public void calculateDuplicates() {

        // Count duplicates, select it, sort by length and reverse words
        Map<String, Integer> mapDuplicatesWords = countDuplicatesWords(fileName);
        List<String> selectDuplicates = selectDuplicatesWords(mapDuplicatesWords, duplicatesWordsQuantity);
        List<String> sortDuplicates = sortWordsByLength(selectDuplicates);
        List<String> reverseDuplicates = reverseWordsInList(sortDuplicates);

        reverseDuplicates.forEach(item -> System.out.println(item.toUpperCase()));
    }

    private Map<String, Integer> countDuplicatesWords(String fileName) {

        Map<String, Integer> map = new HashMap<>();
        File fileWithText = new File(fileName);
        try (Scanner scanner = new Scanner(fileWithText)) {
            while (scanner.hasNext()) {
                String[] words = scanner.nextLine().split(delimitersInText);

                // Finding duplicates in text
                for (String str : words) {
                    if (!map.containsKey(str)) {
                        map.put(str, 1);
                    } else {
                        map.put(str, map.get(str) + 1);
                    }
                }
            }
        } catch (FileNotFoundException exception){
            exception.getMessage();
        }
        return map;
    }

    private List<String> selectDuplicatesWords(Map<String, Integer> map, int duplicatesWordsQuantity) {

        return map.entrySet().stream()
                .filter(e -> e.getValue() > 1)
                .map(Map.Entry::getKey)
                .limit(duplicatesWordsQuantity)
                .collect(Collectors.toList());
    }

    private List<String> sortWordsByLength(List<String> duplicates) {

        duplicates.sort(Comparator.comparingInt(String::length));
        return duplicates;
    }

    private List<String> reverseWordsInList(List<String> duplicates){

        return duplicates.stream()
                .map(elem -> new StringBuilder(elem.toUpperCase()).reverse().toString())
                .collect(Collectors.toList());
    }
}
