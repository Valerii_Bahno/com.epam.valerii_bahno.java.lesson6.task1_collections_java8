import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Length {

    private final String fileName = "text";
    private final String delimitersInText = "[\\s+., ]+";
    private final int quantityLongestWords = 3;

    public void calculateLength() {

        Arrays.stream(sortWordsByLength(splitTextOnWords(fileName, quantityLongestWords)))
                .limit(quantityLongestWords)
                .forEach(arrayWords -> System.out.println(arrayWords + " -> " + arrayWords.length()));
    }

    private String[] splitTextOnWords(String fileName, int quantityLongestWords) {

        String[] wordsFromText = new String[quantityLongestWords];
        File fileWithText = new File(fileName);

        try (Scanner scanner = new Scanner(fileWithText)) {
            wordsFromText = scanner.nextLine().split(delimitersInText);
        } catch (FileNotFoundException exception){
            exception.getMessage();
        }
        return wordsFromText;
    }

    private String[] sortWordsByLength(String[] wordsFromText) {

        Arrays.sort(wordsFromText, Comparator.comparing(String::length, Comparator.reverseOrder()));
        return wordsFromText;
    }
}
