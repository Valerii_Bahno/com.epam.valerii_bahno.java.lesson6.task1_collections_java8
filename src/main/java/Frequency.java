import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Frequency {

    private final String fileName = "text";
    private final String delimitersInText = "[\\s+., ]+";
    private final int quantityMostFrequencyWords = 2;
    private final int descendingOrder = -1;

    public void calculateFrequency() {

        Map<String, Integer> listWordsAndQuantity = countWordsAndQuantity(fileName);
        List<Map.Entry<String, Integer>> sortedWordsList = sortWordsByFrequency(listWordsAndQuantity);

        sortedWordsList.stream()
                .limit(quantityMostFrequencyWords)
                .forEach(result -> System.out.println(result.getKey() + " -> " + result.getValue()));
    }

    private Map<String, Integer> countWordsAndQuantity(String fileName) {

        Map<String, Integer> listWordsAndQuantity = new HashMap<>();
        File fileWithText = new File(fileName);
        try (Scanner scanner = new Scanner(fileWithText)) {
            while (scanner.hasNext()) {
                String word = scanner.useDelimiter(delimitersInText).next();
                Integer count = listWordsAndQuantity.get(word);
                if (count == null) {
                    count = 0;
                }
                listWordsAndQuantity.put(word, ++count);
            }
        } catch (FileNotFoundException exception){
            exception.getMessage();
        }
        return listWordsAndQuantity;
    }

    private List<Map.Entry<String, Integer>> sortWordsByFrequency(Map<String, Integer> listWordsAndQuantity) {

        List<Map.Entry<String, Integer>> sortedWordsList = new ArrayList<>(listWordsAndQuantity.entrySet());
        sortedWordsList.sort((a, b) -> descendingOrder * a.getValue().compareTo(b.getValue()));
        return sortedWordsList;
    }
}
